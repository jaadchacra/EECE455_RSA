package com.example.atom.eece455_proj10_rsa.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.atom.eece455_proj10_rsa.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                //I REMOVED THIS AND PUT (0,0) !!!, there was one also in onCreate for when we enter the app
//                this.overridePendingTransition(R.anim.animation_leave2,R.anim.animation_enter2);
                overridePendingTransition(0, 0);

                Intent myIntent = new Intent(getBaseContext(), MainActivity.class);
                startActivity(myIntent);
                overridePendingTransition(0, 0);
//                this.overridePendingTransition(R.anim.animation_enter,R.anim.animation_leave);
//                overridePendingTransition(android.R.anim.slide_out_right, android.R.anim.slide_in_left);

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.action_bar_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView actionbartitle = (TextView) findViewById(R.id.action_bar_title);
        actionbartitle.setText("About Us");

        TextView body = (TextView) findViewById(R.id.aboutusbody);
        body.setMovementMethod(new ScrollingMovementMethod());

    }
}
