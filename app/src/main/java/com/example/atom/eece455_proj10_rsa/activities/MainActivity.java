package com.example.atom.eece455_proj10_rsa.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.widget.TextView;

import com.example.atom.eece455_proj10_rsa.R;
import com.example.atom.eece455_proj10_rsa.fragments.MenuFragment;


public class MainActivity extends Activity {
    String[] menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getActionBar().setCustomView(R.layout.action_bar_title);

        Fragment fragment = new MenuFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment).commit();
        TextView actionbartitle = (TextView) findViewById(R.id.action_bar_title);
        actionbartitle.setText("Main Menu");
    }

}
