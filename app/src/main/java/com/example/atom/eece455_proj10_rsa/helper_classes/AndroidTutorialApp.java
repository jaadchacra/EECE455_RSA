package com.example.atom.eece455_proj10_rsa.helper_classes;

import android.app.Application;

import java.security.PrivateKey;
import java.security.PublicKey;

public class AndroidTutorialApp extends Application {

    //add this variable declaration:
    public static PublicKey uk;
    public static PrivateKey rk;

    private static AndroidTutorialApp singleton;

    public static AndroidTutorialApp getInstance() {
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
    }
}