package com.example.atom.eece455_proj10_rsa.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.atom.eece455_proj10_rsa.R;
import com.example.atom.eece455_proj10_rsa.helper_classes.AndroidTutorialApp;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class GenerateKeyActivity extends AppCompatActivity {

    private final String RSA = "RSA";
    SharedPreferences prefs;
    Boolean yourLocked;
    TextView textPublicKey, textConfirm;
    TextView textPrivateKey;
    ProgressBar progressBarCenter;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                //I REMOVED THIS AND PUT (0,0) !!!, there was one also in onCreate for when we enter the app
//                this.overridePendingTransition(R.anim.animation_leave2,R.anim.animation_enter2);
                overridePendingTransition(0, 0);

                Intent myIntent = new Intent(getBaseContext(), MainActivity.class);
                startActivity(myIntent);
                overridePendingTransition(0, 0);
//                this.overridePendingTransition(R.anim.animation_enter,R.anim.animation_leave);
//                overridePendingTransition(android.R.anim.slide_out_right, android.R.anim.slide_in_left);

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_key);

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.action_bar_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView actionbartitle = (TextView) findViewById(R.id.action_bar_title);
        actionbartitle.setText("Key Generation");

        List<String> spinnerArray =  new ArrayList<>();
        spinnerArray.add("1024");
        spinnerArray.add("2048");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        final Spinner sItems = (Spinner) findViewById(R.id.spinner);
        sItems.setAdapter(adapter);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.edit().putBoolean("2048", false).commit();
        yourLocked = prefs.getBoolean("2048", false);

        textPublicKey = (TextView) findViewById(R.id.text_public_key);
        textPrivateKey = (TextView) findViewById(R.id.text_private_key);
        final Button button = (Button) findViewById(R.id.button);

        textConfirm = (TextView) findViewById(R.id.text_confirm);


        progressBarCenter = (ProgressBar) findViewById(R.id.progressbar_center);
        progressBarCenter.setVisibility(View.GONE);
        textPublicKey.setVisibility(View.GONE);
        textPrivateKey.setVisibility(View.GONE);
        textConfirm.setVisibility(View.GONE);

        final TextView spinnerLabel = (TextView) findViewById(R.id.text_label_spinner);

        sItems.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (position ==1) {
                    prefs.edit().putBoolean("2048", true).commit();
                }
                else {
                    prefs.edit().putBoolean("2048", false).commit();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        button.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    spinnerLabel.setVisibility(View.GONE);
                    sItems.setVisibility(View.GONE);
                    button.setVisibility(View.GONE);
                    textPublicKey.setVisibility(View.GONE);
                    textPrivateKey.setVisibility(View.GONE);
                    textConfirm.setVisibility(View.GONE);
                    progressBarCenter.setVisibility(View.VISIBLE);
                    generateKey(yourLocked);
                } catch (Exception e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

                textPublicKey.setText("PUBLIC KEY: " + AndroidTutorialApp.uk.toString());
                textPrivateKey.setText("PRIVATE KEY: " + AndroidTutorialApp.rk.toString());
                progressBarCenter.setVisibility(View.GONE);
                spinnerLabel.setVisibility(View.VISIBLE);
                sItems.setVisibility(View.VISIBLE);
                button.setVisibility(View.VISIBLE);
                textPublicKey.setVisibility(View.VISIBLE);
                textPrivateKey.setVisibility(View.VISIBLE);
                textConfirm.setVisibility(View.VISIBLE);
                return false;
            }
        });


        button.setText("Generate Keys");

    }

    public void generateKey(boolean yourLocked) throws Exception {
        KeyPairGenerator gen = KeyPairGenerator.getInstance(RSA);
        if(yourLocked==true) {
            gen.initialize(2048, new SecureRandom());
        }
        else {
            gen.initialize(1024, new SecureRandom());
        }
        KeyPair keyPair = gen.generateKeyPair();
        AndroidTutorialApp.uk = keyPair.getPublic();
        AndroidTutorialApp.rk = keyPair.getPrivate();
    }

    public class LoadData extends AsyncTask<Void, Void, Void> {
        ProgressDialog progressDialog;
        //declare other objects as per your need
        @Override
        protected void onPreExecute()
        {
            progressDialog= ProgressDialog.show(GenerateKeyActivity.this, "Progress Dialog Title Text","Process Description Text", true);

            //do initialization of required objects objects here
        };
        @Override
        protected Void doInBackground(Void... params)
        {

            //do loading operation here
            return null;
        }
        @Override
        protected void onPostExecute(Void result)
        {
            super.onPostExecute(result);
            progressDialog.dismiss();
        };
    }

}


