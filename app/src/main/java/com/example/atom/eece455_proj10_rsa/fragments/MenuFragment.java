package com.example.atom.eece455_proj10_rsa.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.atom.eece455_proj10_rsa.R;
import com.example.atom.eece455_proj10_rsa.activities.AboutActivity;
import com.example.atom.eece455_proj10_rsa.activities.DecryptActivity;
import com.example.atom.eece455_proj10_rsa.activities.EncryptActivity;
import com.example.atom.eece455_proj10_rsa.activities.GenerateKeyActivity;
import com.szugyi.circlemenu.view.CircleImageView;
import com.szugyi.circlemenu.view.CircleLayout;

public class MenuFragment extends Fragment implements CircleLayout.OnItemClickListener {

    public static final String ARG_LAYOUT = "layout";

    protected CircleLayout circleLayout;

    public MenuFragment() {
        // Empty constructor required for fragment subclasses
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container,
                false);


        // Set listeners
        circleLayout = (CircleLayout) rootView.findViewById(R.id.circle_layout);
        circleLayout.setOnItemClickListener(this);

        ImageView centerImage = (ImageView) rootView.findViewById(R.id.myImageView);
        centerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), AboutActivity.class);
                startActivity(myIntent);
            }
        });

        String name = null;
        View view = circleLayout.getSelectedItem();
        if (view instanceof CircleImageView) {
            name = ((CircleImageView) view).getName();
        }
        return rootView;
    }

    @Override
    public void onItemClick(View view) {
        Intent myIntent;
        switch (view.getId()) {
            case R.id.main_generate_key_image:
                myIntent = new Intent(getActivity(), GenerateKeyActivity.class);
                startActivity(myIntent);
                break;
            case R.id.main_encrypt_image:
                myIntent = new Intent(getActivity(), EncryptActivity.class);
                startActivity(myIntent);
                break;
            case R.id.main_decrypt_image:
                myIntent = new Intent(getActivity(), DecryptActivity.class);
                startActivity(myIntent);
                break;
        }
    }


    }