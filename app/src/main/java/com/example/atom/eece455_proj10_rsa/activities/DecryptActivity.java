package com.example.atom.eece455_proj10_rsa.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.atom.eece455_proj10_rsa.R;
import com.example.atom.eece455_proj10_rsa.helper_classes.AndroidTutorialApp;

import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.Cipher;

public class DecryptActivity extends AppCompatActivity {

    public static PublicKey uk;
    public static PrivateKey rk;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                //I REMOVED THIS AND PUT (0,0) !!!, there was one also in onCreate for when we enter the app
//                this.overridePendingTransition(R.anim.animation_leave2,R.anim.animation_enter2);
                overridePendingTransition(0, 0);

                    Intent myIntent = new Intent(getBaseContext(), MainActivity.class);
                    startActivity(myIntent);
                    overridePendingTransition(0, 0);
//                this.overridePendingTransition(R.anim.animation_enter,R.anim.animation_leave);
//                overridePendingTransition(android.R.anim.slide_out_right, android.R.anim.slide_in_left);

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decrypt);
        //To remove Keyboard on touch
        setupUI(findViewById(R.id.rel_decrypt));

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.action_bar_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView actionbartitle = (TextView) findViewById(R.id.action_bar_title);
        actionbartitle.setText("Decryption");

        final Button copyButton = (Button) findViewById(R.id.button_copy);
        final Button deButton = (Button) findViewById(R.id.debutton);
        final EditText input = (EditText) findViewById(R.id.Input);
        final TextView output = (TextView) findViewById(R.id.OriginText);

        final TextView textMessage = (TextView) findViewById(R.id.error_message);
        final Button butotnMessage = (Button) findViewById(R.id.retry_button);

        uk = AndroidTutorialApp.uk;
        rk = AndroidTutorialApp.rk;

        copyButton.setVisibility(View.GONE);
        output.setVisibility(View.GONE);
        input.setVisibility(View.GONE);
        deButton.setVisibility(View.GONE);

        textMessage.setVisibility(View.GONE);
        butotnMessage.setVisibility(View.GONE);

        if (uk != null) {
            input.setVisibility(View.VISIBLE);
            deButton.setVisibility(View.VISIBLE);

            deButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    try {
                        if(input.getText().toString().equals("")) {
                            Toast.makeText(DecryptActivity.this, "Please insert text to decrypt", Toast.LENGTH_SHORT).show();
                        } else{
                            output.setText(String.valueOf(decrypt(input.getText().toString())));
                            output.setVisibility(View.VISIBLE);
                            copyButton.setVisibility(View.VISIBLE);
                        }

                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            });

            copyButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    String stringYouExtracted = output.getText().toString();
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                    clipboard.setText(stringYouExtracted);
                    Toast.makeText(DecryptActivity.this, "Copied to Clipboard", Toast.LENGTH_SHORT).show();
                }
            });
        } else{
            textMessage.setVisibility(View.VISIBLE);
            butotnMessage.setVisibility(View.VISIBLE);
            butotnMessage.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    finish();
                    //I REMOVED THIS AND PUT (0,0) !!!, there was one also in onCreate for when we enter the app
//                this.overridePendingTransition(R.anim.animation_leave2,R.anim.animation_enter2);
                    overridePendingTransition(0, 0);

                    Intent myIntent = new Intent(getBaseContext(), GenerateKeyActivity.class);
                    startActivity(myIntent);
                    overridePendingTransition(0, 0);

                }
            });


        }
    }




    private final static String RSA = "RSA";

    public final String decrypt(String data) {
        try {
            return new String(decrypt(hex2byte(data.getBytes())));
        } catch (Exception e) {
            e.printStackTrace();
            // please enter key in key page
        }
        return null;
    }


    private byte[] decrypt(byte[] src) throws Exception {
        Cipher cipher = Cipher.getInstance(RSA);
        cipher.init(Cipher.DECRYPT_MODE, rk);
        return cipher.doFinal(src);
    }



    public byte[] hex2byte(byte[] b) {
        if ((b.length % 2) != 0)
            throw new IllegalArgumentException("hello");
        byte[] b2 = new byte[b.length / 2];
        for (int n = 0; n < b.length; n += 2) {
            String item = new String(b, n, 2);
            b2[n / 2] = (byte) Integer.parseInt(item, 16);
        }
        return b2;
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (NullPointerException err) {

        }
    }
    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(DecryptActivity.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }
}
